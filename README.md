# Go Mod

```shell
go mod tidy
```

# Comandos coverage

  ```shell
  go test ./...
  ```

  ```shell
  go test -coverprofile=coverage.out ./... && go tool cover -func=coverage.out
  ```

  ```shell
  go test ./... --coverprofile=cover.out && go tool cover --html=cover.out
  ```

# Comando para compilar

  ```shell
  go build
  ```

# arquivo de configuração para teste
file: ".env-test"
PORT_SERVER=8080
DATABASE_MONGODB_URL=mongodb://admin:admin123@127.0.0.1:27017


# Instalando Protoc
```shell
sudo apt install -y protobuf-compiler
sudo apt install golang-goprotobuf-dev
```

## Configurar as Variáveis
```shell
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH="$PATH:/usr/bin/protoc"
export PATH="$PATH:$GOROOT:$GOPATH:$GOBIN"
```

## Compilara os aquivos proto
```shell
protoc --proto_path=proto proto/*.proto --go_out=plugins=grpc:protobuffer
```

# Instalar o Evans para teste o servidor grpc
https://github.com/ktr0731/evans
```shell
go install github.com/ktr0731/evans@latest
```
## Comandos Evans
Mesma porta do arquivo cmd/server/main.go
```shell
evans -r -p 50055
service QrCodeService
```