package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/ciwee/back-end/shop/product-service/graph/generated"
	"gitlab.com/ciwee/back-end/shop/product-service/graph/model"
)

// CreateProduct is the resolver for the CreateProduct field.
func (r *mutationResolver) CreateProduct(ctx context.Context, product model.CreateProductInput) (*model.CreateProductOutput, error) {
	panic(fmt.Errorf("not implemented: CreateProduct - CreateProduct"))
}

// UpdateProduct is the resolver for the UpdateProduct field.
func (r *mutationResolver) UpdateProduct(ctx context.Context, id string, product model.UpdateProductInput) (*model.UpdateProductOutput, error) {
	panic(fmt.Errorf("not implemented: UpdateProduct - UpdateProduct"))
}

// FindByID is the resolver for the FindById field.
func (r *queryResolver) FindByID(ctx context.Context, id string) (*model.Product, error) {
	panic(fmt.Errorf("not implemented: FindByID - FindById"))
}

// FindAll is the resolver for the FindAll field.
func (r *queryResolver) FindAll(ctx context.Context) ([]*model.Product, error) {
	panic(fmt.Errorf("not implemented: FindAll - FindAll"))
}

// ActivateOrDeactivate is the resolver for the ActivateOrDeactivate field.
func (r *queryResolver) ActivateOrDeactivate(ctx context.Context) (*model.QueryOutput, error) {
	panic(fmt.Errorf("not implemented: ActivateOrDeactivate - ActivateOrDeactivate"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
