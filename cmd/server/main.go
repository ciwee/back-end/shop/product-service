package main

import (
  "gitlab.com/ciwee/back-end/shop/product-service/cmd/usecase"
  "gitlab.com/ciwee/back-end/shop/product-service/protobuffer"
  "google.golang.org/grpc"
  "google.golang.org/grpc/reflection"
  "log"
  "net"
)

func main() {
  grpcServer := grpc.NewServer()
  protobuffer.RegisterProductServiceServer(grpcServer, &usecase.ServiceGRPC{})
  reflection.Register(grpcServer) // habilitar o acesso do evans

  listener, err := net.Listen("tcp", ":50055")
  if err != nil {
    log.Fatalf(err.Error())
  }
  grpcServer.Serve(listener)
}
