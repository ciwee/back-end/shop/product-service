package main

import (
	"context"
	"gitlab.com/ciwee/back-end/shop/product-service/protobuffer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

func main() {
	connection, err := grpc.Dial("localhost:50055", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer connection.Close()

	client := protobuffer.NewQrCodeServiceClient(connection)
	request := &protobuffer.QRCodeRequest{
		Description: "Macbook",
		ImageLink:   "https://s3.imagem/resource/6a1c4bc2-049d-418a-9131-be412939e7f0",
		Uuid:        "6a1c4bc2-049d-418a-9131-be412939e7f0",
	}

	response, err := client.Create(context.Background(), request)

	if err != nil {
		log.Fatalf(err.Error())
	}
	log.Println(response.LinkQrcode)

	//// stream
	//request2 := &protobuffer.FibonacciRequest{
	//  Number: 20,
	//}
	//
	//responseStream, err := client.Fibonacci(context.Background(), request2)
	//
	//for {
	//  stream, err := responseStream.Recv()
	//  if err == io.EOF {
	//    break
	//  }
	//  log.Printf("Fribonacci: %v", stream.GetResult())
	//}
}
