package usecase

import (
  "context"
  "gitlab.com/ciwee/back-end/shop/product-service/protobuffer"
  framework_database "gitlab.com/ciwee/back-end/shop/product-service/src/framework/database"
  framework_properties "gitlab.com/ciwee/back-end/shop/product-service/src/framework/properties"
  "gitlab.com/ciwee/back-end/shop/product-service/src/framework/repository/products"
  qrcode2 "gitlab.com/ciwee/back-end/shop/product-service/src/framework/repository/qrcode"
  "gitlab.com/ciwee/back-end/shop/product-service/src/interface_adapters/presenter"
  "gitlab.com/ciwee/back-end/shop/product-service/src/interface_adapters/repository/product"
  "gitlab.com/ciwee/back-end/shop/product-service/src/interface_adapters/repository/qrcode"
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases"
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
  "os"
  "regexp"
)

type ServiceGRPC struct {
}

func (qr *ServiceGRPC) Create(ctx context.Context, input *protobuffer.CreateProductInput) (*protobuffer.CreateProductOutput, error) {

  projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
  currentWorkDirectory, _ := os.Getwd()
  rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

  newProperties := framework_properties.NewProperties(rootPath)
  mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())
  productRepository := products.NewProductRepository(mongoDataSource)
  productAdapterRepository := product.NewProductAdapterRepository(productRepository)

  productPresenter := presenter.NewProductPresenter()
  qrCodeRepository := qrcode2.NewQrCodeRepository(newProperties.Load())
  qrCodeAdapterRepository := qrcode.NewQrCodeAdapterRepository(qrCodeRepository)
  productInteractor := use_cases.NewProductInteractor(productAdapterRepository, productPresenter, qrCodeAdapterRepository)

  response := productInteractor.Create(dto.ProductCreate{
    Description: input.Description,
    ImageLink:   input.ImageLink,
    EnableGrid:  input.EnableGrid,
  })

  err := response.Error
  return &protobuffer.CreateProductOutput{
    Message: response.Message,
    Error:   err.Error(),
  }, nil
}

func (qr *ServiceGRPC) Update(ctx context.Context, input *protobuffer.UpdateProductInput) (*protobuffer.UpdateProductOutput, error) {

  //TODO implement me
  panic("implement me")
}
