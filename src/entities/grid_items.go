package entities

import (
	"errors"
)

type GridItems struct {
	description string
	acronym     string
	sequencer   uint64
}

func (g GridItems) Description() string {
	return g.description
}

func (g GridItems) Acronym() string {
	return g.acronym
}

func (g GridItems) Sequencer() uint64 {
	return g.sequencer
}

func (g GridItems) IsValid() (bool, error) {
	if g.description == "" {
		return false, errors.New("grid item description not populated")
	}
	return true, nil
}

func NewGridItem(description string, acronym string, sequencer uint64) IGridItems {
	return GridItems{
		description: description,
		acronym:     acronym,
		sequencer:   sequencer,
	}
}
