package entities

import (
	"errors"
)

type Grid struct {
	typeGrid string
	items    []IGridItems
}

func (g Grid) Type() string {
	return g.typeGrid
}

func (g Grid) Items() []IGridItems {
	return g.items
}

func (g Grid) IsValid() (bool, error) {
	if g.typeGrid == "" {
		return false, errors.New("grid description not completed")
	}
	if len(g.items) == 0 {
		return false, errors.New("grid items not populated")
	} else {
		for _, item := range g.items {
			isValid, err := item.IsValid()
			if err != nil {
				return isValid, err
			}
		}
	}
	return true, nil
}

func NewGrid(typeGrid string, items []IGridItems) IGrid {
	return Grid{
		typeGrid: typeGrid,
		items:    items,
	}
}
