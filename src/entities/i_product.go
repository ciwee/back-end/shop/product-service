package entities

type IProduct interface {
	Description() string
	EnableGrid() bool
	Grids() []IGrid
	IsValid() (bool, error)
	SetImageLink(imageLink string)
	GetImageLink() string
	SetQRCodeLink(qrCode string)
	GetQRCodeLink() string
}
