package entities

type IGridItems interface {
	Description() string
	Acronym() string
	Sequencer() uint64
	IsValid() (bool, error)
}
