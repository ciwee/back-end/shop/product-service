package entities

import (
	"errors"
)

type Product struct {
	description string
	enableGrid  bool
	grids       []IGrid
	imageLink   string
	qrCodeLink  string
}

func (p Product) Description() string {
	return p.description
}

func (p Product) EnableGrid() bool {
	return p.enableGrid
}

func (p Product) Grids() []IGrid {
	return p.grids
}

func (p *Product) SetImageLink(imageLink string) {
	p.imageLink = imageLink
}

func (p Product) GetImageLink() string {
	return p.imageLink
}

func (p *Product) SetQRCodeLink(qrCode string) {
	p.qrCodeLink = qrCode
}
func (p Product) GetQRCodeLink() string {
	return p.qrCodeLink
}

func (p Product) IsValid() (bool, error) {
	switch {
	case p.description == "":
		return false, errors.New("product description not completed")
	case p.imageLink == "":
		return false, errors.New("product image not completed")
	case p.enableGrid:
		if len(p.grids) == 0 {
			return false, errors.New("product grades not completed")
		}
		for _, item := range p.grids {
			isValid, err := item.IsValid()
			if err != nil {
				return isValid, err
			}
		}
	}

	return true, nil
}

func NewProduct(description string) IProduct {
	return &Product{
		description: description,
		enableGrid:  false,
	}
}

func NewProductGrid(description string, imageLink string, enableGrid bool, grids []IGrid) IProduct {
	return &Product{
		description: description,
		enableGrid:  enableGrid,
		grids:       grids,
		imageLink:   imageLink,
	}
}
