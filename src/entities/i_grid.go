package entities

type IGrid interface {
	Type() string
	Items() []IGridItems
	IsValid() (bool, error)
}
