package entities

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_devera_validar_produto_sem_descricao(t *testing.T) {

	assert := assert.New(t)

	product := NewProduct("")
	valid, err := product.IsValid()

	assert.NotNil(product)
	assert.Empty(product.Description())
	assert.False(valid)
	assert.EqualError(err, "product description not completed")

}

func Test_devera_validar_produto_grade_sem_itens_da_graded(t *testing.T) {

	assert := assert.New(t)

	product := NewProductGrid("Mac book", "", true, []IGrid{})
	product.SetImageLink("https://s3.qrcode-b69db8de-2415-486c-abcb-33938f315df7")
	product.SetQRCodeLink("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5")
	valid, err := product.IsValid()

	assert.NotNil(product)
	assert.NotEmpty(product.Description)
	assert.Equal("Mac book", product.Description())
	assert.Equal("https://s3.qrcode-b69db8de-2415-486c-abcb-33938f315df7", product.GetImageLink())
	assert.Equal("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5", product.GetQRCodeLink())
	assert.False(valid)
	assert.EqualError(err, "product grades not completed")
}

func Test_devera_validar_com_sucesso_o_produto(t *testing.T) {

	assert := assert.New(t)

	cor1 := NewGridItem("Azul", "A", 1)
	cor2 := NewGridItem("Rosa", "R", 2)
	cor3 := NewGridItem("Cinza", "C", 3)
	cores := NewGrid("Cor", []IGridItems{cor1, cor2, cor3})

	tamanho1 := NewGridItem("Pequeno", "P", 1)
	tamanho2 := NewGridItem("Medio", "M", 2)
	tamanho3 := NewGridItem("Grande", "G", 3)
	tamanho4 := NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := NewGrid("Tamanho", []IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := NewProductGrid("Mac book", "", true, []IGrid{cores, tamanhos})
	productGrid.SetImageLink("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b")
	productGrid.SetQRCodeLink("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5")

	valid, err := productGrid.IsValid()

	assert.NotNil(productGrid)
	assert.NotEmpty(productGrid.Description())
	assert.Equal("Mac book", productGrid.Description())
	assert.Equal("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", productGrid.GetImageLink())
	assert.Equal("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5", productGrid.GetQRCodeLink())
	assert.Equal(true, productGrid.EnableGrid())
	assert.True(valid)
	assert.NoError(err)
	assert.Len(productGrid.Grids(), 2)
	assert.Equal("Cor", productGrid.Grids()[0].Type())
	assert.Equal("Tamanho", productGrid.Grids()[1].Type())
	assert.Len(productGrid.Grids()[0].Items(), 3)
	assert.Len(productGrid.Grids()[1].Items(), 5)
	assert.Equal("Azul", productGrid.Grids()[0].Items()[0].Description())
	assert.Equal(uint64(1), productGrid.Grids()[0].Items()[0].Sequencer())
	assert.Equal("A", productGrid.Grids()[0].Items()[0].Acronym())

}

func Test_devera_validar_produto_com_grade_sem_tipo(t *testing.T) {

	assert := assert.New(t)

	cor1 := NewGridItem("Azul", "A", 1)
	cor2 := NewGridItem("Rosa", "R", 2)
	cor3 := NewGridItem("Cinza", "C", 3)
	cores := NewGrid("Cor", []IGridItems{cor1, cor2, cor3})

	tamanho1 := NewGridItem("Pequeno", "P", 1)
	tamanho2 := NewGridItem("Medio", "M", 2)
	tamanho3 := NewGridItem("Grande", "G", 3)
	tamanho4 := NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := NewGrid("", []IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := NewProductGrid("Mac book", "", true, []IGrid{cores, tamanhos})
	productGrid.SetImageLink("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b")
	productGrid.SetQRCodeLink("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5")

	valid, err := productGrid.IsValid()

	assert.NotNil(productGrid)
	assert.NotEmpty(productGrid.Description)
	assert.Equal("Mac book", productGrid.Description())
	assert.Equal("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", productGrid.GetImageLink())
	assert.Equal("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5", productGrid.GetQRCodeLink())
	assert.Equal(true, productGrid.EnableGrid())
	assert.False(valid)
	assert.EqualError(err, "grid description not completed")

}

func Test_devera_validar_produto_com_grade_com_item_sem_descricao(t *testing.T) {

	assert := assert.New(t)

	cor1 := NewGridItem("Azul", "A", 1)
	cor2 := NewGridItem("Rosa", "R", 2)
	cor3 := NewGridItem("Cinza", "C", 3)
	cores := NewGrid("Cor", []IGridItems{cor1, cor2, cor3})

	tamanho1 := NewGridItem("", "P", 1)
	tamanho2 := NewGridItem("Medio", "M", 2)
	tamanho3 := NewGridItem("Grande", "G", 3)
	tamanho4 := NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := NewGrid("Tamanho", []IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := NewProductGrid("Mac book", "", true, []IGrid{cores, tamanhos})
	productGrid.SetImageLink("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b")
	productGrid.SetQRCodeLink("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5")

	valid, err := productGrid.IsValid()

	assert.NotNil(productGrid)
	assert.NotEmpty(productGrid.Description)
	assert.Equal("Mac book", productGrid.Description())
	assert.Equal("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", productGrid.GetImageLink())
	assert.Equal("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5", productGrid.GetQRCodeLink())
	assert.Equal(true, productGrid.EnableGrid())
	assert.False(valid)
	assert.EqualError(err, "grid item description not populated")

}

func Test_devera_validar_produto_com_grade_sem_itens_da_grade(t *testing.T) {

	assert := assert.New(t)

	cores := NewGrid("Cor", []IGridItems{})

	productGrid := NewProductGrid("Mac book", "", true, []IGrid{cores})
	productGrid.SetImageLink("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b")
	productGrid.SetQRCodeLink("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5")

	valid, err := productGrid.IsValid()

	assert.NotNil(productGrid)
	assert.NotEmpty(productGrid.Description)
	assert.Equal("Mac book", productGrid.Description())
	assert.Equal("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", productGrid.GetImageLink())
	assert.Equal("https://s3.qrcode-0612a539-41c5-4a39-9bb5-6e4186f118b5", productGrid.GetQRCodeLink())
	assert.Equal(true, productGrid.EnableGrid())
	assert.False(valid)
	assert.EqualError(err, "grid items not populated")

}

func Test_devera_validar_o_image_do_produto(t *testing.T) {

	assert := assert.New(t)

	cor1 := NewGridItem("Azul", "A", 1)
	cor2 := NewGridItem("Rosa", "R", 2)
	cor3 := NewGridItem("Cinza", "C", 3)
	cores := NewGrid("Cor", []IGridItems{cor1, cor2, cor3})

	tamanho1 := NewGridItem("Pequeno", "P", 1)
	tamanho2 := NewGridItem("Medio", "M", 2)
	tamanho3 := NewGridItem("Grande", "G", 3)
	tamanho4 := NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := NewGrid("Tamanho", []IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := NewProductGrid("Mac book", "", true, []IGrid{cores, tamanhos})
	productGrid.SetQRCodeLink("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b")

	valid, err := productGrid.IsValid()

	assert.NotNil(productGrid)
	assert.NotEmpty(productGrid.Description())
	assert.Equal("Mac book", productGrid.Description())
	assert.Equal("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", productGrid.GetQRCodeLink())
	assert.Equal(true, productGrid.EnableGrid())
	assert.False(valid)
	assert.EqualError(err, "product image not completed")

}
