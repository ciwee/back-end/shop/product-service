package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/response"
)

type MockProductPresenter struct {
	mock.Mock
}

func (mock MockProductPresenter) Success(message string) response.MessageResponse {
	args := mock.Called()
	result := args.Get(0)
	return result.(response.MessageResponse)

}

func (mock MockProductPresenter) Fail(err error) response.MessageResponse {
	args := mock.Called()
	result := args.Get(0)
	return result.(response.MessageResponse)
}

func (mock *MockProductPresenter) FindAll(mapper *mapper.PaginationProductMapper, err error) (
	[]response.ProductResponse,
	error,
) {
	args := mock.Called()
	result := args.Get(0)
	return result.([]response.ProductResponse), args.Error(1)
}

func (mock *MockProductPresenter) FindById(productMapper *mapper.ProductMapper, err error) (
	response.ProductResponse,
	error,
) {
	args := mock.Called()
	result := args.Get(0)
	return result.(response.ProductResponse), args.Error(1)
}
