package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/ciwee/back-end/shop/product-service/src/entities"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
)

type MockProductAdapterRepository struct {
	mock.Mock
}

func (mock *MockProductAdapterRepository) Save(product entities.IProduct) error {
	args := mock.Called()
	return args.Error(0)
}
func (mock *MockProductAdapterRepository) Update(id string, product entities.IProduct) error {
	args := mock.Called()
	return args.Error(0)
}

func (mock *MockProductAdapterRepository) FindAll(page int, limit int) (*mapper.PaginationProductMapper, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(*mapper.PaginationProductMapper), args.Error(1)
}

func (mock *MockProductAdapterRepository) FindById(id string) (*mapper.ProductMapper, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(*mapper.ProductMapper), args.Error(1)
}

func (mock *MockProductAdapterRepository) ActivateOrDeactivate(id string, status bool) error {
	args := mock.Called()
	return args.Error(0)
}
