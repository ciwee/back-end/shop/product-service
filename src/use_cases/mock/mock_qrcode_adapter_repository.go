package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type MockQrCodeAdapterRepository struct {
	mock.Mock
}

func (mock *MockQrCodeAdapterRepository) Create(product dto.ProductCreate) (string, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(string), args.Error(1)
}

func (mock *MockQrCodeAdapterRepository) RemoveAndCreate(uuid string, product dto.ProductCreate) (string, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(string), args.Error(1)
}
