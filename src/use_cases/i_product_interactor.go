package use_cases

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/response"
)

type IProductInteractor interface {
	Create(productRequest dto.ProductCreate) response.MessageResponse
	Update(id string, productRequest dto.ProductCreate) response.MessageResponse
	FindAll(page int, limit int) ([]response.ProductResponse, error)
	FindById(id string) (response.ProductResponse, error)
	ActivateOrDeactivate(id string, status bool) response.MessageResponse
}
