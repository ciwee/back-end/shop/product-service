package use_cases

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/entities"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
)

type IProductAdapterRepository interface {
	Save(product entities.IProduct) error
	FindAll(page int, limit int) (*mapper.PaginationProductMapper, error)
	FindById(id string) (*mapper.ProductMapper, error)
	ActivateOrDeactivate(id string, status bool) error
	Update(id string, product entities.IProduct) error
}
