package use_cases

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/response"
)

type IProductPresenter interface {
	Success(message string) response.MessageResponse
	Fail(err error) response.MessageResponse
	FindAll(mapper *mapper.PaginationProductMapper, err error) ([]response.ProductResponse, error)
	FindById(productMapper *mapper.ProductMapper, err error) (response.ProductResponse, error)
}
