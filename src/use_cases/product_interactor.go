package use_cases

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/response"
)

type ProductInteractor struct {
	productRepository IProductAdapterRepository
	productPresenter  IProductPresenter
	qrCodeRepository  IQrCodeAdapterRepository
}

func (p ProductInteractor) Create(productRequest dto.ProductCreate) response.MessageResponse {
	var product = productRequest.ToEntity()
	_, err := product.IsValid()
	if err != nil {
		return p.productPresenter.Fail(err)
	}
	qrcode, err := p.qrCodeRepository.Create(productRequest)
	product.SetQRCodeLink(qrcode)
	if err != nil {
		return p.productPresenter.Fail(err)
	}
	errRep := p.productRepository.Save(product)
	if errRep != nil {
		return p.productPresenter.Fail(err)
	}
	return p.productPresenter.Success(product.Description() + " " + "product, is registered")
}

func (p ProductInteractor) Update(id string, productRequest dto.ProductCreate) response.MessageResponse {
	var product = productRequest.ToEntity()
	_, err := product.IsValid()
	if err != nil {
		return p.productPresenter.Fail(err)
	}
	qrCodeLink, err := p.qrCodeRepository.RemoveAndCreate(id, productRequest)
	product.SetQRCodeLink(qrCodeLink)
	if err != nil {
		return p.productPresenter.Fail(err)
	}
	errRep := p.productRepository.Update(id, product)
	if errRep != nil {
		return p.productPresenter.Fail(err)
	}

	return p.productPresenter.Success(product.Description() + " " + "product data, have been changed")
}

func (p ProductInteractor) FindAll(page int, limit int) ([]response.ProductResponse, error) {
	productsMapper, err := p.productRepository.FindAll(page, limit)
	return p.productPresenter.FindAll(productsMapper, err)
}

func (p ProductInteractor) FindById(id string) (response.ProductResponse, error) {
	productMapper, err := p.productRepository.FindById(id)
	return p.productPresenter.FindById(productMapper, err)
}

func (p ProductInteractor) ActivateOrDeactivate(id string, status bool) response.MessageResponse {
	err := p.productRepository.ActivateOrDeactivate(id, status)
	if err != nil {
		return p.productPresenter.Fail(err)
	}
	return p.productPresenter.Success("product status has changed")
}

func NewProductInteractor(
	productRepository IProductAdapterRepository,
	productPresenter IProductPresenter,
	qrCodeRepository IQrCodeAdapterRepository,
) IProductInteractor {
	return ProductInteractor{
		productRepository: productRepository,
		productPresenter:  productPresenter,
		qrCodeRepository:  qrCodeRepository,
	}
}
