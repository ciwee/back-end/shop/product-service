package use_cases

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type IQrCodeAdapterRepository interface {
	Create(product dto.ProductCreate) (string, error)
	RemoveAndCreate(uuid string, product dto.ProductCreate) (string, error)
}
