package mapper

type GridItemMapper struct {
	Description string `bson:"description"`
	Acronym     string `bson:"acronym"`
	Sequencer   uint64 `bson:"sequencer"`
}
