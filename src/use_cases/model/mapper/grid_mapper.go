package mapper

type GridMapper struct {
	TypeGrid string           `bson:"type_grid"`
	Items    []GridItemMapper `bson:"items"`
}
