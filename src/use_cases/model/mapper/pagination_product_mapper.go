package mapper

type PaginationProductMapper struct {
	Total     int64           `json:"total,omitempty"`
	Page      int64           `json:"page,omitempty"`
	PerPage   int64           `json:"per_page,omitempty"`
	Prev      int64           `json:"prev,omitempty"`
	Next      int64           `json:"next,omitempty"`
	TotalPage int64           `json:"total_page,omitempty"`
	Products  []ProductMapper `json:"products,omitempty"`
}
