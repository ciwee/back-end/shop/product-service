package mapper

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ProductMapper struct {
	Id          primitive.ObjectID `bson:"_id"`
	Description string             `bson:"description"`
	EnableGrid  bool               `bson:"enable_grid"`
	Grids       []GridMapper       `bson:"grids"`
	CreatedAt   time.Time          `bson:"created_at"`
	Status      bool               `bson:"status"`
}
