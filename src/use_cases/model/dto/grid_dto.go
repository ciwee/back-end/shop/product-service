package dto

type GridDto struct {
	TypeGrid string        `json:"type_grid,omitempty"`
	Items    []GridItemDto `json:"items,omitempty"`
}

func NewGriRequest(typeGrid string, items []GridItemDto) GridDto {
	return GridDto{TypeGrid: typeGrid, Items: items}
}
