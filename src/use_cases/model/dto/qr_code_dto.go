package dto

type QrCodeDto struct {
	Uuid        string
	ImageLink   string
	Description string
}
