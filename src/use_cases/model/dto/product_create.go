package dto

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/entities"
)

type ProductCreate struct {
	Description string    `json:"description,omitempty"`
	EnableGrid  bool      `json:"enable_grid,omitempty"`
	Grids       []GridDto `json:"grids,omitempty"`
	ImageLink   string    `json:"image_link,omitempty"`
}

func (product ProductCreate) ToEntity() entities.IProduct {
	var grids []entities.IGrid
	for _, grid := range product.Grids {
		var itemsGrid []entities.IGridItems
		for _, item := range grid.Items {
			itemsGrid = append(itemsGrid, entities.NewGridItem(item.Description, item.Acronym, item.Sequencer))
		}
		grids = append(grids, entities.NewGrid(grid.TypeGrid, itemsGrid))
	}
	return entities.NewProductGrid(product.Description, product.ImageLink, product.EnableGrid, grids)
}

func NewProductDto(description string, imageLink string, enableGrid bool, grids []GridDto) ProductCreate {
	return ProductCreate{
		Description: description,
		ImageLink:   imageLink,
		EnableGrid:  enableGrid,
		Grids:       grids,
	}
}
