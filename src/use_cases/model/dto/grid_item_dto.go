package dto

type GridItemDto struct {
	Description string `json:"description,omitempty"`
	Acronym     string `json:"acronym,omitempty"`
	Sequencer   uint64 `json:"sequencer,omitempty"`
}

func NewGridItemRequest(description string, acronym string, sequencer uint64) GridItemDto {
	return GridItemDto{Description: description, Acronym: acronym, Sequencer: sequencer}
}
