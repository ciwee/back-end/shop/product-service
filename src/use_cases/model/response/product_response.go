package response

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type ProductResponse struct {
	dto.ProductCreate
	Id        string `json:"id,omitempty"`
	CreatedAt string `json:"created_at"`
}
