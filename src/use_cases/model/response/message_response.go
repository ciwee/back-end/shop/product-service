package response

type MessageResponse struct {
	Message string `json:"message,omitempty"`
	Error   error  `json:"error,omitempty"`
}
