package response

type PaginationProductResponse struct {
	Total     int64
	Page      int64
	PerPage   int64
	Prev      int64
	Next      int64
	TotalPage int64
	products  []ProductResponse
}
