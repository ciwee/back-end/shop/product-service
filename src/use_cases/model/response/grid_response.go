package response

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type GridResponse struct {
	dto.GridDto
}
