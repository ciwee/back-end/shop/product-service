package use_cases

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/mock"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/response"
)

func Test_devera_salvar_produto_com_sucesso(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gridItemRequest = dto.NewGridItemRequest("Azul", "AZ", uint64(1))
	var gripRequest = dto.NewGriRequest("Cor", []dto.GridItemDto{gridItemRequest})
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca10f3g",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("Create").Return("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", nil)
	productAdapterRepository.On("Save").Return(nil)
	productPresenter.On("Success").Return(
		response.MessageResponse{
			Message: "Macbook product, is registered",
		},
	)

	productResponse := productInteractor.Create(productRequest)

	assert.NotNil(productResponse)
	assert.Equal("Macbook product, is registered", productResponse.Message)
	assert.NoError(productResponse.Error)

}

func Test_devera_salvar_produto_sem_grade(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		false,
		nil,
	)

	qrCodeAdapterRepository.On("Create").Return("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", nil)
	productAdapterRepository.On("Save").Return(nil)
	productPresenter.On("Success").Return(
		response.MessageResponse{
			Message: "Macbook product, is registered",
		},
	)

	productResponse := productInteractor.Create(productRequest)

	assert.NotNil(productResponse)
	assert.Equal("Macbook product, is registered", productResponse.Message)
	assert.NoError(productResponse.Error)

}

func Test_devera_validar_produto_com_grades_sem_itens_da_grade(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gripRequest = dto.NewGriRequest("Cor", nil)
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("Create").Return("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", nil)
	productAdapterRepository.On("Save").Return(nil)
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("grid items not populated"),
		},
	)

	productResponse := productInteractor.Create(productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "grid items not populated")

}

func Test_devera_validar_caso_ocorre_erro_ao_salvar(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gridItemRequest = dto.NewGridItemRequest("Azul", "AZ", uint64(1))
	var gripRequest = dto.NewGriRequest("Cor", []dto.GridItemDto{gridItemRequest})
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("Create").Return("https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b", nil)
	productAdapterRepository.On("Save").Return(errors.New("erro inesperado"))
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("erro inesperado"),
		},
	)

	productResponse := productInteractor.Create(productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "erro inesperado")
}

func Test_devera_buscar_produtos_paginados(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)

	productAdapterRepository.On("FindAll").Return(&mapper.PaginationProductMapper{}, nil)
	productPresenter.On("FindAll").Return(
		[]response.ProductResponse{
			{
				Id: "bf7f07bf497b304be8ee34dff",
				ProductCreate: dto.ProductCreate{
					Description: "Macbook",
					EnableGrid:  true,
					Grids: []dto.GridDto{
						{
							TypeGrid: "Cor",
							Items: []dto.GridItemDto{
								{
									Description: "Azul",
									Acronym:     "A",
									Sequencer:   uint64(1),
								},
							},
						},
					},
				},
			},
		},
		nil,
	)

	productsResponse, err := productInteractor.FindAll(1, 10)

	assert.NotNil(productsResponse)
	assert.NoError(err)
	assert.Len(productsResponse, 1)
	assert.Equal("Macbook", productsResponse[0].Description)
	assert.Equal("bf7f07bf497b304be8ee34dff", productsResponse[0].Id)
	assert.Equal(true, productsResponse[0].EnableGrid)
	assert.Len(productsResponse[0].Grids, 1)
	assert.Equal("Cor", productsResponse[0].Grids[0].TypeGrid)
	assert.Len(productsResponse[0].Grids[0].Items, 1)
	assert.Equal(productsResponse[0].Grids[0].Items[0].Description, "Azul")
	assert.Equal(productsResponse[0].Grids[0].Items[0].Acronym, "A")
	assert.Equal(productsResponse[0].Grids[0].Items[0].Sequencer, uint64(1))
}

func Test_devera_validar_em_casso_de_erro_durante_a_busca(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)

	productAdapterRepository.On("FindAll").Return(&mapper.PaginationProductMapper{}, errors.New("erro inesperado"))
	productPresenter.On("FindAll").Return([]response.ProductResponse{}, errors.New("erro inesperado"))

	productsResponse, err := productInteractor.FindAll(1, 10)

	assert.NotNil(productsResponse)
	assert.EqualError(err, "erro inesperado")
	assert.Len(productsResponse, 0)
}

func Test_devera_buscar_produto_por_id(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)

	productAdapterRepository.On("FindById").Return(&mapper.ProductMapper{}, nil)
	productPresenter.On("FindById").Return(
		response.ProductResponse{
			Id: "bf7f07bf497b304be8ee34dff",
			ProductCreate: dto.ProductCreate{
				Description: "Macbook",
				EnableGrid:  true,
				Grids: []dto.GridDto{
					{
						TypeGrid: "Cor",
						Items: []dto.GridItemDto{
							{
								Description: "Azul",
								Acronym:     "A",
								Sequencer:   uint64(1),
							},
						},
					},
				},
			},
		},
		nil,
	)

	productResponse, err := productInteractor.FindById("bf7f07bf497b304be8ee34dff")

	assert.NotNil(productResponse)
	assert.NoError(err)
	assert.Equal("Macbook", productResponse.Description)
	assert.Equal("bf7f07bf497b304be8ee34dff", productResponse.Id)
	assert.Equal(true, productResponse.EnableGrid)
	assert.Len(productResponse.Grids, 1)
	assert.Equal("Cor", productResponse.Grids[0].TypeGrid)
	assert.Len(productResponse.Grids[0].Items, 1)
	assert.Equal(productResponse.Grids[0].Items[0].Description, "Azul")
	assert.Equal(productResponse.Grids[0].Items[0].Acronym, "A")
	assert.Equal(productResponse.Grids[0].Items[0].Sequencer, uint64(1))
}

func Test_devera_validar_erro_ao_buscar_um_produto(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)

	productAdapterRepository.On("FindById").Return(&mapper.ProductMapper{}, errors.New("erro inesperado"))
	productPresenter.On("FindById").Return(response.ProductResponse{}, errors.New("erro inesperado"))

	productResponse, err := productInteractor.FindById("bf7f07bf497b304be8ee34dff")

	assert.NotNil(productResponse)
	assert.EqualError(err, "erro inesperado")
}

func Test_devera_inativar_o_produto(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)

	productAdapterRepository.On("ActivateOrDeactivate").Return(nil)
	productPresenter.On("Success").Return(
		response.MessageResponse{
			Message: "product status has changed",
		},
	)

	productResponse := productInteractor.ActivateOrDeactivate("bf7f07bf497b304be8ee34dff", true)

	assert.NotNil(productResponse)
	assert.NoError(productResponse.Error)
	assert.Equal("product status has changed", productResponse.Message)
}

func Test_devera_validar_erro_ao_inativar_o_produto(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)

	productAdapterRepository.On("ActivateOrDeactivate").Return(errors.New("erro inesperado"))
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("erro inesperado"),
		},
	)

	productResponse := productInteractor.ActivateOrDeactivate("bf7f07bf497b304be8ee34dff", true)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "erro inesperado")
	assert.Empty(productResponse.Message)
}

func Test_devera_alterar_os_dados_do_produto(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gridItemRequest = dto.NewGridItemRequest("Azul", "AZ", uint64(1))
	var gripRequest = dto.NewGriRequest("Cor", []dto.GridItemDto{gridItemRequest})
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("RemoveAndCreate").Return(
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		nil,
	)
	productAdapterRepository.On("Update").Return(nil)
	productPresenter.On("Success").Return(
		response.MessageResponse{
			Message: "Macbook product data, have been changed",
		},
	)

	productResponse := productInteractor.Update("bf7f07bf497b304be8ee34dff", productRequest)

	assert.NotNil(productResponse)
	assert.Equal("Macbook product data, have been changed", productResponse.Message)
	assert.NoError(productResponse.Error)
}

func Test_devera_validar_caso_ocorre_erro_ao_alterar_produto(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gridItemRequest = dto.NewGridItemRequest("Azul", "AZ", uint64(1))
	var gripRequest = dto.NewGriRequest("Cor", []dto.GridItemDto{gridItemRequest})
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("RemoveAndCreate").Return(
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		nil,
	)
	productAdapterRepository.On("Update").Return(errors.New("erro inesperado"))
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("erro inesperado"),
		},
	)

	productResponse := productInteractor.Update("bf7f07bf497b304be8ee34dff", productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "erro inesperado")
}

func Test_devera_validar_produto_com_grades_sem_itens_da_grade_ao_alterar(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		true,
		[]dto.GridDto{},
	)

	qrCodeAdapterRepository.On("RemoveAndCreate").Return(
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		nil,
	)
	productAdapterRepository.On("Update").Return(nil)
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("grid items not populated"),
		},
	)

	productResponse := productInteractor.Update("bf7f07bf497b304be8ee34dff", productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "grid items not populated")

}

func Test_devera_validar_image_do_produto(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca99f2b",
		true,
		[]dto.GridDto{},
	)

	qrCodeAdapterRepository.On("RemoveAndCreate").Return("", errors.New("imagem não contrada"))
	productAdapterRepository.On("Update").Return(nil)
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("imagem não contrada"),
		},
	)

	productResponse := productInteractor.Update("bf7f07bf497b304be8ee34dff", productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "imagem não contrada")

}

func Test_devera_validar_image_do_produto_ao_alterar(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gridItemRequest = dto.NewGridItemRequest("Azul", "AZ", uint64(1))
	var gripRequest = dto.NewGriRequest("Cor", []dto.GridItemDto{gridItemRequest})
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca10f3g",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("RemoveAndCreate").Return("", errors.New("imagem não contrada"))
	productAdapterRepository.On("Update").Return(nil)
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("imagem não contrada"),
		},
	)

	productResponse := productInteractor.Update("bf7f07bf497b304be8ee34dff", productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "imagem não contrada")

}

func Test_devera_validar_image_do_produto_ao_salvar(t *testing.T) {
	assert := assert.New(t)

	productAdapterRepository := new(mock.MockProductAdapterRepository)
	productPresenter := new(mock.MockProductPresenter)
	qrCodeAdapterRepository := new(mock.MockQrCodeAdapterRepository)

	var productInteractor = NewProductInteractor(
		productAdapterRepository,
		productPresenter,
		qrCodeAdapterRepository,
	)
	var gridItemRequest = dto.NewGridItemRequest("Azul", "AZ", uint64(1))
	var gripRequest = dto.NewGriRequest("Cor", []dto.GridItemDto{gridItemRequest})
	var productRequest = dto.NewProductDto(
		"Macbook",
		"https://s3.link/qrcode-c5862ecb-11a3-457e-b4c7-238c9ca10f3g",
		true,
		[]dto.GridDto{gripRequest},
	)

	qrCodeAdapterRepository.On("Create").Return("", errors.New("imagem não contrada"))
	productAdapterRepository.On("Save").Return(nil)
	productPresenter.On("Fail").Return(
		response.MessageResponse{
			Error: errors.New("imagem não contrada"),
		},
	)

	productResponse := productInteractor.Create(productRequest)

	assert.NotNil(productResponse)
	assert.EqualError(productResponse.Error, "imagem não contrada")

}
