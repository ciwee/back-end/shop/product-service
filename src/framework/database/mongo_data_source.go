package framework_database

import (
	"context"
	"log"

	"gitlab.com/ciwee/back-end/shop/product-service/src/framework/properties"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoDataSource struct {
	clientOptions *options.ClientOptions
}

func initClientOptions(properties framework_properties.PropertiesResponse) *options.ClientOptions {
	return options.Client().ApplyURI(properties.DataBaseMongoUrl)
}

func (c *mongoDataSource) Connect() (*mongo.Client, error) {
	ctx := context.TODO()
	client, err := mongo.Connect(ctx, c.clientOptions)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	log.Println("Connection to MongoDB opened.")
	return client, nil
}

func (c *mongoDataSource) DataSource(client *mongo.Client, database, collection string) *mongo.Collection {
	return client.Database(database).Collection(collection)
}

func (c *mongoDataSource) Disconnect(client *mongo.Client) (bool, error) {
	err := client.Disconnect(context.TODO())
	if err != nil {
		log.Println(err)
		return false, err
	}
	log.Println("Connection to MongoDB closed.")
	return true, nil
}

func NewMongoDataSource(properties framework_properties.PropertiesResponse) IMongoDataSource {
	return &mongoDataSource{clientOptions: initClientOptions(properties)}
}
