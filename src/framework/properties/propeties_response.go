package framework_properties

type PropertiesResponse struct {
	DataBaseMongoUrl     string
	PortServer           string
	GrpcClientQrCodeHost string
	GrpcClientQrCodePort string
}
