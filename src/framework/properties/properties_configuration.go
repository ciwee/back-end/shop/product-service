package framework_properties

import (
	"os"

	"github.com/joho/godotenv"
)

type propertiesConfiguration struct {
	path string
}

func NewProperties(path string) IProperties {
	return propertiesConfiguration{path: path}
}

func (p propertiesConfiguration) Load() PropertiesResponse {
	godotenv.Load(os.ExpandEnv(p.path))
	return PropertiesResponse{
		DataBaseMongoUrl:     os.Getenv("DATABASE_MONGODB_URL"),
		PortServer:           os.Getenv("PORT_SERVER"),
		GrpcClientQrCodeHost: os.Getenv("GRPC_CLIENTE_QRCODE_HOST"),
		GrpcClientQrCodePort: os.Getenv("GRPC_CLIENTE_QRCODE_PORT"),
	}
}
