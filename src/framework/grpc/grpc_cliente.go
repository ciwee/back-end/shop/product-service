package grpc

import (
	framework_properties "gitlab.com/ciwee/back-end/shop/product-service/src/framework/properties"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

type GRPCClient struct {
	HostAndPort string
	connection  *grpc.ClientConn
}

func (g *GRPCClient) Connect() *grpc.ClientConn {
	connection, err := grpc.Dial(g.HostAndPort, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf(err.Error())
	}
	g.connection = connection
	return g.connection
}

func (g *GRPCClient) Close() {
	g.connection.Close()
}

func NewGRPCCliente(properties framework_properties.PropertiesResponse) *GRPCClient {
	return &GRPCClient{
		HostAndPort: properties.GrpcClientQrCodeHost + ":" + properties.GrpcClientQrCodePort,
	}
}
