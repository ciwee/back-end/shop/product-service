package products

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/framework/database"
	"gitlab.com/ciwee/back-end/shop/product-service/src/interface_adapters/repository/product"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
)

type productRepository struct {
	genericRepository[mapper.ProductMapper]
}

func (c productRepository) Save(mapper mapper.ProductMapper) (*string, error) {
	return c.genericRepository.Save(mapper)
}

func (c productRepository) FindById(id string) (*mapper.ProductMapper, error) {
	return c.genericRepository.FindById(id)
}

func (c productRepository) FindAll(page int, limit int) (*mapper.PaginationProductMapper, error) {
	paginatedData, productsMapper, err := c.genericRepository.FindAll(page, limit)
	if err != nil {
		return nil, err
	}
	return &mapper.PaginationProductMapper{
		Products:  productsMapper,
		Next:      paginatedData.Pagination.Next,
		Page:      paginatedData.Pagination.Page,
		Prev:      paginatedData.Pagination.Prev,
		Total:     paginatedData.Pagination.Total,
		PerPage:   paginatedData.Pagination.PerPage,
		TotalPage: paginatedData.Pagination.TotalPage,
	}, nil
}

func (c productRepository) Update(productMapper mapper.ProductMapper) error {
	return c.genericRepository.Update(productMapper.Id, productMapper)
}

func NewProductRepository(
	mongoDataSource framework_database.IMongoDataSource,
) product.IProductRepository {
	return productRepository{
		genericRepository[mapper.ProductMapper]{
			mongoDataSource: mongoDataSource, databaseName: "lukeware", tableName: "produtos",
		},
	}
}

func NewProductRepositoryCustom(
	mongoDataSource framework_database.IMongoDataSource,
	databaseName string,
	tableName string,
) product.IProductRepository {
	return productRepository{
		genericRepository[mapper.ProductMapper]{
			mongoDataSource: mongoDataSource, databaseName: databaseName, tableName: tableName,
		},
	}
}
