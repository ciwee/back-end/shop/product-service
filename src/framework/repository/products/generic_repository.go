package products

import (
	"context"
	"time"

	. "github.com/gobeam/mongo-go-pagination"
	"gitlab.com/ciwee/back-end/shop/product-service/src/framework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type genericRepository[T any] struct {
	mongoDataSource framework_database.IMongoDataSource
	databaseName    string
	tableName       string
}

func (c genericRepository[T]) Save(mapper T) (*string, error) {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, c.databaseName, c.tableName)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	response, err := collection.InsertOne(ctx, mapper)
	if err != nil {
		return nil, err
	}
	hex := response.InsertedID.(primitive.ObjectID).Hex()
	return &hex, nil
}

func (c genericRepository[T]) FindById(id string) (*T, error) {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, c.databaseName, c.tableName)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	var mapper T
	objectID, _ := primitive.ObjectIDFromHex(id)
	err := collection.FindOne(ctx, bson.D{{"_id", objectID}}).Decode(&mapper)

	if err != nil {
		return nil, err
	}
	return &mapper, nil
}

func (c genericRepository[T]) FindAll(page int, limit int) (
	*PaginatedData,
	[]T,
	error,
) {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, c.databaseName, c.tableName)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var mappers []T
	paginatedData, err := New(collection).Context(ctx).Limit(int64(limit)).Filter(bson.M{}).Page(int64(page)).Decode(&mappers).Find()
	if err != nil {
		return nil, nil, err
	}
	return paginatedData, mappers, nil
}

func (c genericRepository[T]) Update(id primitive.ObjectID, mapper interface{}) error {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, c.databaseName, c.tableName)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	_, err := collection.UpdateByID(ctx, id, bson.M{"$set": mapper})
	if err != nil {
		return err
	}
	return nil

}
