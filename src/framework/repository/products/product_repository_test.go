package products

import (
	"context"
	"os"
	"regexp"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ciwee/back-end/shop/product-service/src/framework/database"
	"gitlab.com/ciwee/back-end/shop/product-service/src/framework/properties"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func Test_devera_salvar_um_produto(t *testing.T) {
	defer cleanBase()
	assert := assert.New(t)
	projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
	currentWorkDirectory, _ := os.Getwd()
	rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

	product :=
			mapper.ProductMapper{
				Id:          primitive.NewObjectID(),
				Description: "Macbook",
				EnableGrid:  true,
				Grids: []mapper.GridMapper{
					{
						TypeGrid: "Cor",
						Items: []mapper.GridItemMapper{
							{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
						},
					},
				},
				CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
				Status:    true,
			}

	newProperties := framework_properties.NewProperties(rootPath)
	mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())
	productRepository := NewProductRepositoryCustom(mongoDataSource, "lukeware-test", "produtos")

	id, err := productRepository.Save(product)

	assert.Nil(err)
	assert.NoError(err)
	assert.NotNil(id)
	assert.True(len(*id) > 0)

}

func Test_devera_buscar_produto_por_id(t *testing.T) {
	defer cleanBase()
	assert := assert.New(t)
	projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
	currentWorkDirectory, _ := os.Getwd()
	rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

	product :=
			mapper.ProductMapper{
				Id:          primitive.NewObjectID(),
				Description: "Macbook",
				EnableGrid:  true,
				Grids: []mapper.GridMapper{
					{
						TypeGrid: "Cor",
						Items: []mapper.GridItemMapper{
							{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
						},
					},
				},
				CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
				Status:    true,
			}

	newProperties := framework_properties.NewProperties(rootPath)
	mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())
	productRepository := NewProductRepositoryCustom(mongoDataSource, "lukeware-test", "produtos")

	id, err1 := productRepository.Save(product)
	productMapper, err2 := productRepository.FindById(*id)

	assert.Nil(err1)
	assert.NoError(err1)
	assert.Nil(err2)
	assert.NoError(err2)
	assert.NotNil(productMapper)
}

func Test_devera_lista_todos_produtos_com_paginacao(t *testing.T) {
	defer cleanBase()
	assert := assert.New(t)
	projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
	currentWorkDirectory, _ := os.Getwd()
	rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

	product :=
			mapper.ProductMapper{
				Id:          primitive.NewObjectID(),
				Description: "Macbook",
				EnableGrid:  true,
				Grids: []mapper.GridMapper{
					{
						TypeGrid: "Cor",
						Items: []mapper.GridItemMapper{
							{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
						},
					},
				},
				CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
				Status:    true,
			}

	newProperties := framework_properties.NewProperties(rootPath)
	mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())
	productRepository := NewProductRepositoryCustom(mongoDataSource, "lukeware-test", "produtos")

	id, err1 := productRepository.Save(product)
	paginationProduct, err2 := productRepository.FindAll(0, 10)

	assert.Nil(err1)
	assert.NoError(err1)
	assert.Nil(err2)
	assert.NoError(err2)
	assert.NotNil(id)
	assert.NotNil(paginationProduct)
	assert.True(paginationProduct.TotalPage > int64(0))
	assert.True(paginationProduct.Page > int64(0))
	assert.True(paginationProduct.Prev == int64(0))
	assert.True(paginationProduct.Next == int64(0))
	assert.True(paginationProduct.TotalPage == int64(1))
	assert.Len(paginationProduct.Products, 1)

}

func Test_devera_atualizar_o_produto(t *testing.T) {
	defer cleanBase()
	assert := assert.New(t)
	projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
	currentWorkDirectory, _ := os.Getwd()
	rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

	product :=
			mapper.ProductMapper{
				Id:          primitive.NewObjectID(),
				Description: "Macbook",
				EnableGrid:  true,
				Grids: []mapper.GridMapper{
					{
						TypeGrid: "Cor",
						Items: []mapper.GridItemMapper{
							{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
						},
					},
				},
				CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
				Status:    true,
			}

	newProperties := framework_properties.NewProperties(rootPath)
	mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())
	productRepository := NewProductRepositoryCustom(mongoDataSource, "lukeware-test", "produtos")

	id, err1 := productRepository.Save(product)

	product.Description = "Novo Produto"
	err1 = productRepository.Update(product)
	paginationProduct, err2 := productRepository.FindAll(0, 10)

	assert.Nil(err1)
	assert.NotNil(id)
	assert.NoError(err1)

	assert.Nil(err1)
	assert.NoError(err1)
	assert.Nil(err2)
	assert.NoError(err2)
	assert.NotNil(id)
	assert.NotNil(paginationProduct)
	assert.True(paginationProduct.TotalPage > int64(0))
	assert.True(paginationProduct.Page > int64(0))
	assert.True(paginationProduct.Prev == int64(0))
	assert.True(paginationProduct.Next == int64(0))
	assert.True(paginationProduct.TotalPage == int64(1))
	assert.Len(paginationProduct.Products, 1)
	assert.Equal("Novo Produto", paginationProduct.Products[0].Description)

}

func Test_devera_salvar_um_produto_no_banco_default(t *testing.T) {
	defer cleanBase()
	assert := assert.New(t)
	projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
	currentWorkDirectory, _ := os.Getwd()
	rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

	product :=
			mapper.ProductMapper{
				Id:          primitive.NewObjectID(),
				Description: "Macbook",
				EnableGrid:  true,
				Grids: []mapper.GridMapper{
					{
						TypeGrid: "Cor",
						Items: []mapper.GridItemMapper{
							{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
						},
					},
				},
				CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
				Status:    true,
			}

	newProperties := framework_properties.NewProperties(rootPath)
	mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())
	productRepository := NewProductRepository(mongoDataSource)

	id, err := productRepository.Save(product)

	assert.Nil(err)
	assert.NoError(err)
	assert.NotNil(id)

}

func cleanBase() {
	projectName := regexp.MustCompile(`^(.*` + "product-service" + `)`)
	currentWorkDirectory, _ := os.Getwd()
	rootPath := string(projectName.Find([]byte(currentWorkDirectory))) + "/.env-test"

	newProperties := framework_properties.NewProperties(rootPath)
	mongoDataSource := framework_database.NewMongoDataSource(newProperties.Load())

	connect, _ := mongoDataSource.Connect()
	defer mongoDataSource.Disconnect(connect)
	collection := mongoDataSource.DataSource(connect, "lukeware-test", "produtos")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection.Drop(ctx)

	collection = mongoDataSource.DataSource(connect, "lukeware", "produtos")
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)
	collection.Drop(ctx)
}
