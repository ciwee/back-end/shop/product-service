package qrcode

import (
	"context"
	"gitlab.com/ciwee/back-end/shop/product-service/protobuffer"
	"gitlab.com/ciwee/back-end/shop/product-service/src/framework/grpc"
	framework_properties "gitlab.com/ciwee/back-end/shop/product-service/src/framework/properties"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type QrCodeRepository struct {
	grpcClient *grpc.GRPCClient
}

func (q QrCodeRepository) Create(qrCode dto.QrCodeDto) (string, error) {
	connection := q.grpcClient.Connect()
	defer q.grpcClient.Close()
	request := &protobuffer.QRCodeRequest{
		Uuid:        qrCode.Uuid,
		Description: qrCode.Description,
		ImageLink:   qrCode.ImageLink,
	}
	client := protobuffer.NewQrCodeServiceClient(connection)
	response, err := client.Create(context.Background(), request)
	return response.LinkQrcode, err
}

func (q QrCodeRepository) RemoveAndCreate(uuidOld string, qrCode dto.QrCodeDto) (string, error) {
	connection := q.grpcClient.Connect()
	defer q.grpcClient.Close()
	request := &protobuffer.QRCodeRemoveRequest{
		UuidNew:     qrCode.Uuid,
		UuidOld:     uuidOld,
		Description: qrCode.Description,
		ImageLink:   qrCode.ImageLink,
	}
	client := protobuffer.NewQrCodeServiceClient(connection)
	response, err := client.RemoveAndCreate(context.Background(), request)
	return response.LinkQrcode, err
}

func NewQrCodeRepository(properties framework_properties.PropertiesResponse) QrCodeRepository {
	return QrCodeRepository{
		grpcClient: grpc.NewGRPCCliente(properties),
	}
}
