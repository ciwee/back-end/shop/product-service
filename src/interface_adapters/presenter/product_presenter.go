package presenter

import (
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases"
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/response"
)

type ProductPresenter struct {
}

func (p ProductPresenter) Success(message string) response.MessageResponse {
  panic("")
}
func (p ProductPresenter) Fail(err error) response.MessageResponse {
  panic("")
}
func (p ProductPresenter) FindAll(mapper *mapper.PaginationProductMapper, err error) ([]response.ProductResponse, error) {
  panic("")
}
func (p ProductPresenter) FindById(productMapper *mapper.ProductMapper, err error) (response.ProductResponse, error) {
  panic("")
}

func NewProductPresenter() use_cases.IProductPresenter {
  return ProductPresenter{}
}
