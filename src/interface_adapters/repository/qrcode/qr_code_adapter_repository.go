package qrcode

import (
  Uuid "github.com/google/uuid"
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases"
  "gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type QrCodeAdapterRepository struct {
  qrCodeRepository IQrCodeRepository
}

func (q QrCodeAdapterRepository) Create(product dto.ProductCreate) (string, error) {
  qrcode := dto.QrCodeDto{
    Description: product.Description,
    ImageLink:   product.ImageLink,
    Uuid:        Uuid.New().String(),
  }
  return q.qrCodeRepository.Create(qrcode)
}

func (q QrCodeAdapterRepository) RemoveAndCreate(uuid string, product dto.ProductCreate) (string, error) {
  qrcode := dto.QrCodeDto{
    Description: product.Description,
    ImageLink:   product.ImageLink,
    Uuid:        Uuid.New().String(),
  }
  return q.qrCodeRepository.RemoveAndCreate(uuid, qrcode)
}

func NewQrCodeAdapterRepository(qrCodeRepository IQrCodeRepository) use_cases.IQrCodeAdapterRepository {
  return QrCodeAdapterRepository{
    qrCodeRepository: qrCodeRepository,
  }
}
