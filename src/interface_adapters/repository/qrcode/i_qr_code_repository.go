package qrcode

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/dto"
)

type IQrCodeRepository interface {
	Create(qrCode dto.QrCodeDto) (string, error)
	RemoveAndCreate(id string, qrCode dto.QrCodeDto) (string, error)
}
