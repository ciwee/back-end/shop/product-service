package product

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/entities"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type productAdapterRepository struct {
	productRepository IProductRepository
}

func (p productAdapterRepository) Save(product entities.IProduct) error {
	_, err := p.productRepository.Save(p.toMapper(product))
	if err != nil {
		return err
	}
	return nil
}

func (p productAdapterRepository) FindAll(page int, limit int) (*mapper.PaginationProductMapper, error) {
	return p.productRepository.FindAll(page, limit)
}

func (p productAdapterRepository) FindById(id string) (*mapper.ProductMapper, error) {
	return p.productRepository.FindById(id)
}

func (p productAdapterRepository) ActivateOrDeactivate(id string, status bool) error {
	productMapper, err := p.FindById(id)
	if err != nil {
		return err
	}
	productMapper.Id, _ = primitive.ObjectIDFromHex(id)
	productMapper.Status = status
	return p.productRepository.Update(*productMapper)
}

func (p productAdapterRepository) Update(id string, product entities.IProduct) error {
	productMapper := p.toMapper(product)
	productMapper.Id, _ = primitive.ObjectIDFromHex(id)
	return p.productRepository.Update(productMapper)
}

func (p productAdapterRepository) toMapper(product entities.IProduct) mapper.ProductMapper {
	var grids []mapper.GridMapper
	for _, grid := range product.Grids() {
		var gridItems []mapper.GridItemMapper
		for _, item := range grid.Items() {
			gridItems = append(
				gridItems, mapper.GridItemMapper{
					Description: item.Description(),
					Acronym:     item.Acronym(),
					Sequencer:   item.Sequencer(),
				},
			)
		}
		grids = append(
			grids, mapper.GridMapper{
				TypeGrid: grid.Type(),
				Items:    gridItems,
			},
		)
	}

	productMapper := mapper.ProductMapper{
		Description: product.Description(),
		EnableGrid:  product.EnableGrid(),
		Grids:       grids,
	}
	return productMapper
}

func NewProductAdapterRepository(productRepository IProductRepository) use_cases.IProductAdapterRepository {
	return productAdapterRepository{productRepository: productRepository}
}
