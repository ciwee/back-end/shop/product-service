package product

import (
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
)

type IProductRepository interface {
	Save(mapper mapper.ProductMapper) (*string, error)
	FindAll(page int, limit int) (*mapper.PaginationProductMapper, error)
	FindById(id string) (*mapper.ProductMapper, error)
	Update(productMapper mapper.ProductMapper) error
}
