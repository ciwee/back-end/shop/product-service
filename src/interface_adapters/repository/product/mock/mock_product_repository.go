package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
)

type MockProductRepository struct {
	mock.Mock
}

func (mock *MockProductRepository) Save(mapper mapper.ProductMapper) (*string, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(*string), args.Error(1)
}

func (mock *MockProductRepository) FindAll(page int, limit int) (*mapper.PaginationProductMapper, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(*mapper.PaginationProductMapper), args.Error(1)
}

func (mock *MockProductRepository) FindById(id string) (*mapper.ProductMapper, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(*mapper.ProductMapper), args.Error(1)
}

func (mock *MockProductRepository) Update(productMapper mapper.ProductMapper) error {
	args := mock.Called()
	return args.Error(0)
}
