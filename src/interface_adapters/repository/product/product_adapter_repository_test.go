package product

import (
	"errors"
	"gitlab.com/ciwee/back-end/shop/product-service/src/interface_adapters/repository/product/mock"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ciwee/back-end/shop/product-service/src/entities"
	"gitlab.com/ciwee/back-end/shop/product-service/src/use_cases/model/mapper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func Test_devera_salvar_produto(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)

	adapterRepository := NewProductAdapterRepository(productRepository)

	cor1 := entities.NewGridItem("Azul", "A", 1)
	cor2 := entities.NewGridItem("Rosa", "R", 2)
	cor3 := entities.NewGridItem("Cinza", "C", 3)
	cores := entities.NewGrid("Cor", []entities.IGridItems{cor1, cor2, cor3})

	tamanho1 := entities.NewGridItem("Pequeno", "P", 1)
	tamanho2 := entities.NewGridItem("Medio", "M", 2)
	tamanho3 := entities.NewGridItem("Grande", "G", 3)
	tamanho4 := entities.NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := entities.NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := entities.NewGrid("", []entities.IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := entities.NewProductGrid("Mac book", "", true, []entities.IGrid{cores, tamanhos})

	var id = ""
	productRepository.On("Save").Return(&id, nil)

	err := adapterRepository.Save(productGrid)

	assert.NoError(err)

}

func Test_devera_validar_em_caso_de_erro_inesperado(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)

	adapterRepository := NewProductAdapterRepository(productRepository)
	var id = ""
	productRepository.On("Save").Return(&id, errors.New("erro inesperado"))

	cor1 := entities.NewGridItem("Azul", "A", 1)
	cor2 := entities.NewGridItem("Rosa", "R", 2)
	cor3 := entities.NewGridItem("Cinza", "C", 3)
	cores := entities.NewGrid("Cor", []entities.IGridItems{cor1, cor2, cor3})

	size1 := entities.NewGridItem("Pequeno", "P", 1)
	size2 := entities.NewGridItem("Medio", "M", 2)
	size3 := entities.NewGridItem("Grande", "G", 3)
	size4 := entities.NewGridItem("Grande Grande", "GG", 4)
	size5 := entities.NewGridItem("Extra Grande", "EXG", 5)
	sizes := entities.NewGrid("", []entities.IGridItems{size1, size2, size3, size4, size5})

	productGrid := entities.NewProductGrid("Mac book", "", true, []entities.IGrid{cores, sizes})

	err := adapterRepository.Save(productGrid)

	assert.EqualError(err, "erro inesperado")

}

func Test_devera_buscar_uma_lista_de_produtos_paginados(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)

	adapterRepository := NewProductAdapterRepository(productRepository)

	id, _ := primitive.ObjectIDFromHex("bf7f07bf497b304be8ee34df")

	productRepository.On("FindAll").Return(
		&mapper.PaginationProductMapper{
			Products: []mapper.ProductMapper{
				{
					Id:          id,
					Description: "Macbook",
					EnableGrid:  true,
					Grids: []mapper.GridMapper{
						{
							TypeGrid: "Cor",
							Items: []mapper.GridItemMapper{
								{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
							},
						},
					},
					CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
					Status:    true,
				},
			},
		}, nil,
	)

	paginationProducts, err := adapterRepository.FindAll(1, 10)
	products := paginationProducts.Products

	assert.NoError(err)
	assert.NotNil(products)
	assert.Len(products, 1)
	assert.Equal("Macbook", products[0].Description)
	assert.Equal("bf7f07bf497b304be8ee34df", products[0].Id.Hex())
	assert.Equal(true, products[0].EnableGrid)
	assert.Len(products[0].Grids, 1)
	assert.Equal("Cor", products[0].Grids[0].TypeGrid)
	assert.Len(products[0].Grids[0].Items, 1)
	assert.Equal("Azul", products[0].Grids[0].Items[0].Description)
	assert.Equal("A", products[0].Grids[0].Items[0].Acronym)
	assert.Equal(true, products[0].Status)
	assert.Equal(uint64(1), products[0].Grids[0].Items[0].Sequencer)
}

func Test_devera_validar_erro_ao_buscar_uma_lista_de_produtos_paginados(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)

	adapterRepository := NewProductAdapterRepository(productRepository)

	productRepository.On("FindAll").Return(
		&mapper.PaginationProductMapper{}, errors.New("error inesperado"),
	)

	paginationProducts, err := adapterRepository.FindAll(1, 10)

	assert.EqualError(err, "error inesperado")
	assert.NotNil(paginationProducts)
	assert.Nil(paginationProducts.Products)

}

func Test_devera_buscar_um_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)

	adapterRepository := NewProductAdapterRepository(productRepository)

	id, _ := primitive.ObjectIDFromHex("bf7f07bf497b304be8ee34df")

	productRepository.On("FindById").Return(
		&mapper.ProductMapper{
			Id:          id,
			Description: "Macbook",
			EnableGrid:  true,
			Grids: []mapper.GridMapper{
				{
					TypeGrid: "Cor",
					Items: []mapper.GridItemMapper{
						{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
					},
				},
			},
			CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
			Status:    true,
		}, nil,
	)

	product, err := adapterRepository.FindById("bf7f07bf497b304be8ee34dff")

	assert.NoError(err)
	assert.NotNil(product)
	assert.Equal("Macbook", product.Description)
	assert.Equal("bf7f07bf497b304be8ee34df", product.Id.Hex())
	assert.Equal(true, product.EnableGrid)
	assert.Len(product.Grids, 1)
	assert.Equal("Cor", product.Grids[0].TypeGrid)
	assert.Len(product.Grids[0].Items, 1)
	assert.Equal("Azul", product.Grids[0].Items[0].Description)
	assert.Equal("A", product.Grids[0].Items[0].Acronym)
	assert.Equal(true, product.Status)
	assert.Equal(uint64(1), product.Grids[0].Items[0].Sequencer)
}

func Test_devera_validar_erro_ao_buscar_um_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)

	adapterRepository := NewProductAdapterRepository(productRepository)

	productRepository.On("FindById").Return(
		&mapper.ProductMapper{}, errors.New("erro inesperado"),
	)

	product, err := adapterRepository.FindById("bf7f07bf497b304be8ee34df")

	assert.EqualError(err, "erro inesperado")
	assert.NotNil(product)
}

func Test_devera_alterar_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)
	adapterRepository := NewProductAdapterRepository(productRepository)

	cor1 := entities.NewGridItem("Azul", "A", 1)
	cor2 := entities.NewGridItem("Rosa", "R", 2)
	cor3 := entities.NewGridItem("Cinza", "C", 3)
	cores := entities.NewGrid("Cor", []entities.IGridItems{cor1, cor2, cor3})

	tamanho1 := entities.NewGridItem("Pequeno", "P", 1)
	tamanho2 := entities.NewGridItem("Medio", "M", 2)
	tamanho3 := entities.NewGridItem("Grande", "G", 3)
	tamanho4 := entities.NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := entities.NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := entities.NewGrid("", []entities.IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := entities.NewProductGrid("Mac book", "", true, []entities.IGrid{cores, tamanhos})

	productRepository.On("Update").Return(nil)
	err := adapterRepository.Update("bf7f07bf497b304be8ee34df", productGrid)

	assert.NoError(err)
}

func Test_devera_validar_erro_ao_alterar_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)
	adapterRepository := NewProductAdapterRepository(productRepository)

	cor1 := entities.NewGridItem("Azul", "A", 1)
	cor2 := entities.NewGridItem("Rosa", "R", 2)
	cor3 := entities.NewGridItem("Cinza", "C", 3)
	cores := entities.NewGrid("Cor", []entities.IGridItems{cor1, cor2, cor3})

	tamanho1 := entities.NewGridItem("Pequeno", "P", 1)
	tamanho2 := entities.NewGridItem("Medio", "M", 2)
	tamanho3 := entities.NewGridItem("Grande", "G", 3)
	tamanho4 := entities.NewGridItem("Grande Grande", "GG", 4)
	tamanho5 := entities.NewGridItem("Extra Grande", "EXG", 5)
	tamanhos := entities.NewGrid("", []entities.IGridItems{tamanho1, tamanho2, tamanho3, tamanho4, tamanho5})

	productGrid := entities.NewProductGrid("Mac book", "", true, []entities.IGrid{cores, tamanhos})

	productRepository.On("Update").Return(errors.New("erro inesperado"))
	err := adapterRepository.Update("bf7f07bf497b304be8ee34df", productGrid)

	assert.NotNil(err)
	assert.EqualError(err, "erro inesperado")
}

func Test_devera_alterar_estatus_do_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)
	adapterRepository := NewProductAdapterRepository(productRepository)

	id, _ := primitive.ObjectIDFromHex("bf7f07bf497b304be8ee34dff")

	productRepository.On("FindById").Return(
		&mapper.ProductMapper{
			Id:          id,
			Description: "Macbook",
			EnableGrid:  true,
			Grids: []mapper.GridMapper{
				{
					TypeGrid: "Cor",
					Items: []mapper.GridItemMapper{
						{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
					},
				},
			},
			CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
			Status:    false,
		}, nil,
	)
	productRepository.On("Update").Return(nil)
	err := adapterRepository.ActivateOrDeactivate("bf7f07bf497b304be8ee34dff", true)

	assert.NoError(err)
}

func Test_devera_validar_erro_ao_buscar_produto_para_alterar_estatus_do_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)
	adapterRepository := NewProductAdapterRepository(productRepository)

	productRepository.On("FindById").Return(
		&mapper.ProductMapper{}, errors.New("erro ao buscar produto"),
	)
	err := adapterRepository.ActivateOrDeactivate("bf7f07bf497b304be8ee34dff", true)

	assert.EqualError(err, "erro ao buscar produto")
}

func Test_devera_validar_erro_ao_alterar_estatus_do_produto_por_id(t *testing.T) {

	assert := assert.New(t)

	productRepository := new(mock.MockProductRepository)
	adapterRepository := NewProductAdapterRepository(productRepository)

	id, _ := primitive.ObjectIDFromHex("bf7f07bf497b304be8ee34df")

	productRepository.On("FindById").Return(
		&mapper.ProductMapper{
			Id:          id,
			Description: "Macbook",
			EnableGrid:  true,
			Grids: []mapper.GridMapper{
				{
					TypeGrid: "Cor",
					Items: []mapper.GridItemMapper{
						{Description: "Azul", Acronym: "A", Sequencer: uint64(1)},
					},
				},
			},
			CreatedAt: time.Date(2022, 4, 12, 0, 0, 0, 0, time.UTC),
			Status:    false,
		}, nil,
	)
	productRepository.On("Update").Return(errors.New("erro ao alterar o status"))
	err := adapterRepository.ActivateOrDeactivate("bf7f07bf497b304be8ee34dff", true)

	assert.EqualError(err, "erro ao alterar o status")
}
